const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    ollowedUsername: {
    type: String,
    required: true
    },  
    authorId: {
        type: String,
        required: true,
    },
});

const Follow = mongoose.model('Follow', UserSchema);

module.exports = Follow;