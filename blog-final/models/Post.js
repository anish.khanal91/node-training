const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
    title: {
    type: String,
    required: true
    },  
    content: {
        type: String,
        required: true
    },
    postedAt: {
        type: String,
        default: new Date().toString()
    },
    postedBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: false,
        ref: "Author",
      }
});

const Post = mongoose.model('Post', PostSchema);

module.exports = Post;
