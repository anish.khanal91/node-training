const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema({
    coverImageName: {
        path: {
          type: String,
          required: true,
        },
        originalName: {
          type: String,
          required: false,
        },
        mimetype: {
          type: String,
          required: true,
        }
      },
      author: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Author",
      },
    });

const Profile = mongoose.model('Profile', ProfileSchema);

module.exports = Profile;