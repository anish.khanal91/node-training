const express = require('express');
const router = express.Router();
const Post = require('../models/Post');
const postController = require('../controllers/postController');
const userController = require('../controllers/userController');
const followController = require('../controllers/followController');
const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth');
const Profile = require('../models/Profile');

// Welcome Page
router.get('/', forwardAuthenticated, (req, res) => res.render('welcome'));

// index
router.get('/index', ensureAuthenticated, async (req, res) =>{
  const allPosts = await Post.find();

  res.render('index', {
    user: req.user,
    posts: allPosts,
  });
});

// post related routes
router.get('/composePost', postController.viewCreateScreen)
router.post('/composePost', userController.mustBeLoggedIn, postController.create)
router.get('/post/:id', postController.viewSingle)

router.get('/compose', async (req, res) => {
  let profiles
  try {
     profiles = await Profile.find().sort({createAt :'desc'}).limit(10).exec()
  } catch  {
      profiles = []
  }
  res.render('composePost', {
    profile: profiles
  });
});

//follow related routes
// router.post('/addFollow/:username', userController.mustBeLoggedIn, followController.addFollow)

module.exports = router;