const router = require("express").Router();
const Post = require("../models/Post");

router.get("/post/:id", async (req, res) => {
    const { id } = req.params;
    const getPost = await Post.findOne({ _id: id });
    res.render("particularPost", { post: getPost });
  })

  router.get("/delete/:id", (req, res) => {
    const { id } = req.params;
    Post.deleteOne({ _id: id })
      .then(() => {
        res.redirect("/");
      })
      .catch((err) => console.log(err));
  })

  router.get("/edit/:id", async (req, res) => {
    const { id } = req.params;

    const getData = await Post.findOne({ _id: id });
    res.render("editPost", { post: getData });
  })

  router.post("/edit/:id", (req, res) => {
    const { id } = req.params;
    const { title, content } = req.body;

    Post.updateOne({ _id: id }, { title, content })
      .then(() => {
        res.redirect("/");
      })
      .catch((err) => console.log(err));
  });

module.exports = router;