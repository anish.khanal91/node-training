const router = require("express").Router();
const Post = require('../models/Post');
const multer = require('multer');
const upload = multer({ storage: multer.memoryStorage() });

router.get("/compose", (req, res) => {
    res.render("composePost");
  })

  router.post("/compose", upload.single('cover'), (req, res) => {
    const { title, content } = req.body;
    // console.log(title);
    // console.log(content);
    console.log(req.body);
    console.log(req.file);
    // * check for the missing fields!
    if (!title || !content)
      return res.send("Please enter all the required credentials!");

    const newPost = new Post({ title, content });

    // save the post to the database
    newPost
      .save()
      .then(() => {
        console.log("Post Saved Successfully!");
        res.redirect("/");
      })
      .catch((err) => console.log(err));
  });
  router.put("/:id", upload.single("cover"));
module.exports = router;