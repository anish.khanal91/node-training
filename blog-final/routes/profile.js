const express = require("express");
const router = express.Router();
const multer = require("multer");
const path = require("path");
const Profile = require("../models/Profile");
const profileController = require("../controller/profileController");
const uploadPath = path.join("public", Profile.coverImageBasePath);
const imageMimeTypes = ["image/jpeg", "image/png", "image/gif"];
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadPath);
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "--" + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (imageMimeTypes.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
  //upload pic
    // app.post('/upload', (req, res) => {
    //   upload(req, res, (err) => {
    //       if(req.file == undefined){
    //         res.render('index', {
    //           msg: 'Error: No File Selected!'
    //         });
    //       } else {
    //         res.render('index', {
    //           msg: 'File Uploaded!',
    //           file: `uploads/${req.file.filename}`
    //         });
    //       }
    //   });
    // });
const upload = multer({ storage: storage, fileFilter: fileFilter });

// //All Profile Route
router.get("/", requireAuth, allProfile);

//Create Profile route
router.post("/", upload.single("cover"), createProfile);

// router.get("/:id", getProfile);

// router.get("/:id/edit", requireAuth, editBook);

router.put("/:id", upload.single("cover"), requireAuth, updateProfile);


module.exports = router;