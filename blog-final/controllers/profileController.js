const Profile = require("../models/Profile");

exports.createProfile = async (req, res) => {
    let newPath = req.file.path.replace(/[/\/]/g, "/");
    newPath = newPath.slice(6);
  
    const profile = new Profile({
      coverImageName: {
        path: newPath,
        mimetype: req.file.mimetype,
      },
      description: req.body.description,
    });
    try {
      const newProfile = await profile.save();
      res.redirect("/profiles");
    } catch (err) {
      console.log(err);
      if (profile.coverImageName != null) {
        removeProfileCover(profile.coverImage);
      }
      renderNewPage(res, profile, true);
    }
  };
  function removeprofileCover(fileName) {
    fs.unlink(path.join(uploadPath, fileName), (err) => {
      if (err) console.error(err);
    });
  }
  async function renderNewPage(res, profile, hasError = false) {
    renderFormPage(res, profile, "new", hasError);
  }