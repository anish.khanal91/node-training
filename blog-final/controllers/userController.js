exports.mustBeLoggedIn = function(req, res, next) {
    if (req.session.user) {
        next()
    } else {
        req.flash("errors", "You must be logged in to perform that act")
        req.session.save(function() {
            res.redirect('/')
        })
    }
}