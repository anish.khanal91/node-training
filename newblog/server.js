const express = require('express')
const mongoose = require('mongoose')
const articleRouter = require('./routes/articles.js')
const Article = require('./models/article')
const methodOverride = require('method-override')

const app = express()

// mongoose.connect('mongodb+srv://anishkhanal91:anishkhanal91@cluster0.hwax4.mongodb.net/sample_airbnb')
mongoose.connect('mongodb+srv://anishkhanal91:anishkhanal91@cluster0.hwax4.mongodb.net/sample_airbnb').then(() => {
console.log("Connected to Database");
}).catch((err) => {
    console.log("Not Connected to Database ERROR! ", err);
});
app.set('view engine', "ejs")

app.use(methodOverride ("_method"))
app.use(express.urlencoded({ extended :false}))

app.get('/',async(req,res)=>{
   const articles = await Article.find().sort({
       createdAt:"desc"
   })
    res.render('./articles/index', {articles:articles })
})

app.use('/articles',articleRouter)
app.listen(8000, ()=> {
    console.log('server running')
})