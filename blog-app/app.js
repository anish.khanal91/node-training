const express = require("express");
const session = require("express-session");
const User = require("./models/User");
const passport =  require("passport");
const mongoose = require("mongoose");
const localStorage =  require("passport-local");
const app = express();
const router = require('./router');

const userController = require('./controllers/userController')
const {registerUser,setupAdmin, localStorageFun} = require('./controllers/userController')
const {home,login,logout} = require('./Controllers/PostController')

mongoose.connect("mongodb+srv://anishkhanal91:anishkhanal91@cluster0.hwax4.mongodb.net/ComplexApp").then(() => {
    app.listen(8000, () => {
      console.log("Server running on 8000");
      console.log("database connected");
    });
  })
.catch((err) => console.log(err));

app.use(express.urlencoded({extended: false}))
app.use(express.json())

app.use(express.static('public'));

app.set('views', 'views');

app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(
    session({
        secret: "Goodwork",
        resave: false,
        saveUninitialized: true,
    })
);
app.use(express.urlencoded({extended: false}));
app.use(express.json());

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) return next();
    res.redirect("/login");
  }
  
  function isLoggedOut(req, res, next) {
    if (!req.isAuthenticated()) return next();
    res.redirect("/");
  }

app.use(passport.initialize());

app.use(passport.session());

passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {

  User.findById(id, function (err, user) {
    done(err, user);
  });
});

passport.use(
  new localStorage(localStorageFun)
);

app.get('/', userController.home);

app.post('/register', userController.registerUser);


// app.get("/", isLoggedIn, home);

// app.get("/login", isLoggedOut,login);

app.post("/login", passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login?error=true",
  })
);

// app.get("/logout",logout);

app.get("/register", (req, res) => {
  res.render("register");
});
app.get('/login', (req, res) => {
  res.render('login');
});

module.exports = app;